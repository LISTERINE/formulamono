from entityMixins import Movable, Collidable, Entity
import logging

logger = logging.getLogger("character")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class CharacterBase(Entity, Movable, Collidable):
    def __init__(self, *args, **kwargs):
        super(CharacterBase, self).__init__(*args, **kwargs)



