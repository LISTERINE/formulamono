from time import sleep
from curses import endwin, wrapper
from traceback import print_exc
import logging
from sys import exit

from character import CharacterBase
from screenManager import ScreenManager
from inputManager import inputManager

logger = logging.getLogger("engine")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class Game(object):

    def __init__(self):
        self.running = True
        self.entity_map = {}

    def check_colide(self, *args, **kwargs):
        False

    def update(self, input):
        for hash, entity in self.entity_map.items():
            if entity.input_target:
                if input:
                    logger.info("sending input '{}' to entity".format(input))
                    action = entity.send_input(input)
                    if action:
                        if action.get("action_type") == "move":
                            if not self.check_colide(action, self.screen):
                                entity.place(**action)
            if entity.is_drawable:
                entity.panel.window().redrawwin()
        self.screen.update()

    def tick(self):
        key = next(self.input.pump)
        if key == 'q':
            self.quit()
        self.update(key)

    def init_player(self, window):
        logger.info("init player")
        self.player = CharacterBase(input_target=True, window=window)
        if not self.player.check_controls():
            raise Exception("Bad control config")
        self.entity_map[self.player.hash] = self.player
        self.screen.focus_target = self.player
        self.player.panel.top()
        logger.info("player initted")

    def init_screen(self, window):
        logger.info("init screen")
        self.screen = ScreenManager(window)
        self.screen.run()
        logger.info("screen initted")

    def init_input(self):
        logger.info("init input")
        self.input = inputManager()
        self.input.prime_input_pump()
        logger.info("input initted")

    def quit(self):
        if not self.running:
            return
        logger.info("input thread killed")
        try:
            self.input.stop_pump()
        except AttributeError:
            logger.info("issue stopping input pump: {}".format(print_exc()))
            raise
        endwin()
        self.running = False

    def main(self, window):
        logger.info("======new game======")
        try:
            self.init_screen(window)
            self.init_player(self.screen.active_screen.screen)
            self.init_input()
            while self.running:
                self.tick()
                sleep(0.01)
        except Exception:
            logger.info(print_exc())
            self.quit()

if __name__ == "__main__":
    game = Game()
    wrapper(game.main)
