from uuid import uuid4
from collections import Counter
from math import ceil
import curses, curses.panel
import logging

from asciimatics.effects import Sprite, Print
from asciimatics.particles import Rain
from asciimatics.event import KeyboardEvent, MouseEvent
from asciimatics.exceptions import ResizeScreenError
from asciimatics.renderers import StaticRenderer, SpeechBubble, FigletText
from asciimatics.screen import Screen
from asciimatics.paths import DynamicPath
from asciimatics.scene import Scene

logger = logging.getLogger("mixins")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class Entity(object):
    is_entity = True

    def __init__(self, *args, **kwargs):
        super(Entity, self).__init__(*args, **kwargs)
        self.hash = uuid4()
        logger.info("is_entity")

    def update(self):
        pass

class Collidable(object):
    is_collidable = True

    def __init__(self, *args, **kwargs):
        super(Collidable, self).__init__(*args, **kwargs)
        logger.info("is_colliable")

class Drawable(object):
    is_drawable = True

    def __init__(self, y=1, x=1, z=0, window=None, visibility=True, model=None, *args, **kwargs):
        super(Drawable, self).__init__(*args, **kwargs)
        logger.info("is_drawable")
        self.x = x
        self.y = y
        self.z = z
        self.is_visible = visibility
        self.model = model or ["@@","@@"]
        self.height = len(self.model)+1
        self.width = max(map(len, self.model))+1
        self.center = {"y": ceil(float(self.height)/2),
                       "x": ceil(float(self.width)/2)}
        self.panel = self.__make_panel(window)


    @property
    def borders(self):
        return {"tl": (self.y, self.x),
                "tr": (self.y, self.x+self.width),
                "bl": (self.y+self.height, self.x),
                "br": (self.y+self.height, self.x+self.width)}

    def coords(self):
        return {'y':self.y, 'x':self.x, 'z':self.z}

    def __make_panel(self, window):
        if window is None:
            raise Exception("player must be given a parent window reference")
        screen = curses.newwin(self.height, self.width, 2, 2)
        for y, line in enumerate(self.model):
            screen.addnstr(y, 0, line, self.width)
        panel = curses.panel.new_panel(screen)
        return panel




class Movable(Drawable):

    def __init__(self, left_speed=1, right_speed=1, up_speed=1, down_speed=1, input_target=False, max_accelerators=3, max_reversers=1, controls=None, *args, **kwargs):
        logger.info("is_movable")
        super(Movable, self).__init__(*args, **kwargs)
        self.max_accelerators = max_accelerators
        self.max_reversers = max_reversers
        self.left_speed = left_speed
        self.right_speed = right_speed
        self.up_speed = up_speed
        self.down_speed = down_speed
        self.input_target = input_target
        self.orientation = "down"
        self.orientations = ["up", "down", "left", "right"]
        self.move_map = {"left":self.move_left,
                         "right":self.move_right,
                         "up":self.move_up,
                         "down":self.move_down,
                         "forward": self.move_forward,
                         "reverse": self.move_reverse}
        self.reverse_map = {"left": "right",
                            "right": "left",
                            "up": "down",
                            "down": "up"}
        self.controls = controls or {"a": "forward",
                                     "s": "forward",
                                     "d": "forward",
                                     "x": "reverse",
                                     "i": "up",
                                     "j": "left",
                                     "k": "down",
                                     "l": "right"}

    def check_controls(self):
        controls = Counter(self.controls.values())
        try:
            assert 0 < controls.get("forward") >= self.max_accelerators
            assert controls.get("reverse") == self.max_reversers
            assert controls.get("up") == 1
            assert controls.get("down") == 1
            assert controls.get("right") == 1
            assert controls.get("left") == 1
        except AssertionError:
            return False
        return True

    def send_input(self, input):
        if input in self.controls.keys():
            action = self.get_move(self.controls.get(input))
            return dict(action_type="move", **action)

    def place(self, y=0, x=0, z=0, **kwargs):
        self.y = y
        self.x = x
        self.z = z
        logger.info("moving panel to x{}, y{}".format(self.x, self.y))
        self.panel.move(self.y, self.x)
        logger.info("moved and updated")

    def get_move(self, direction, orient=True):
        logger.info("direction={}".format(direction))
        return self.move_map.get(direction)(orient=orient)

    def move_forward(self, orient=False):
        logger.info("moving {}, orienting={}".format("forward", orient))
        return self.get_move(self.orientation, orient)

    def move_reverse(self, orient=False):
        logger.info("moving {}, orienting={}".format("reverse", orient))
        return self.get_move(self.reverse_map.get(self.orientation), orient)

    def move_left(self, orient):
        logger.info("moving {}, orienting={}".format("left", orient))
        if orient:
            self.orientation = "left"
        return {"y":self.y, "x":self.x-self.left_speed, "z":self.z}

    def move_right(self, orient):
        logger.info("moving {}, orienting={}".format("right", orient))
        if orient:
            self.orientation = "right"
        return {"y":self.y, "x":self.x+self.right_speed, "z":self.z}

    def move_up(self, orient):
        logger.info("moving {}, orienting={}".format("up", orient))
        if orient:
            self.orientation = "up"
        return {"y":self.y-self.up_speed, "x":self.x, "z":self.z}

    def move_down(self, orient):
        logger.info("moving {}, orienting={}".format("down", orient))
        if orient:
            self.orientation = "down"
        return {"y":self.y+self.down_speed, "x":self.x, "z":self.z}
