from readchar import readkey
from threading import Thread
import logging
from sys import exit
try:
    from Queue import Queue
except ImportError:
    from queue import Queue

logger = logging.getLogger("inputManager")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class inputManager(object):

    def __init__(self):
        self.input_queue = Queue()
        self.is_running = True
        self.pump = None

    def threaded_input(self):
        try:
            while self.is_running:
                self.input_queue.put(self.get_input())
        except (KeyboardInterrupt, SystemExit):
            logger.info("killing thread")
            exit()

    def input_pump(self):
        while 1:
            if not self.is_running:
                break
            if not self.input_queue.empty():
                yield self.input_queue.get_nowait()
            else:
                yield None

    def fire_input_thread(self):
        self.T = Thread(target=self.threaded_input)
        self.T.start()

    def stop_pump(self):
        logger.info("stopping input thread")
        self.is_running = False
        while self.T.is_alive():
            logger.info("trying to join input thread...")
            self.T.join(0.5)

    def get_input(self):
        return readkey()

    def prime_input_pump(self):
        self.fire_input_thread()
        self.pump = self.input_pump()


if __name__ == "__main__":
    im = inputManager()
    im.prime_input_pump()
    for key in im.pump:
        if key is not None:
            if key == "q":
                im.stop_pump()
