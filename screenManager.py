import curses, curses.panel
import sys
import logging
from math import ceil

logger = logging.getLogger("screenManager")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class Screen(object):
    def __init__(self, screen, max_y, max_x):
        self.screen = screen
        self.max_y = max_y
        self.max_x = max_x

class ScreenManager(object):
    def __init__(self, window):
        self.window_max_y, self.window_max_x = window.getmaxyx()
        self.window_max_y = self.window_max_y-1
        self.window_max_x = self.window_max_x-1
        self.window = Screen(screen=window, max_y=self.window_max_y, max_x=self.window_max_x)
        self.screen_stack = []
        self.vport_x = 0
        self.vport_y = 0
        self.move_translate = {"up": (-1,0),
                               "left": (0,-1),
                               "down": (1,0),
                               "right": (0,1)}

    @property
    def active_screen(self):
        return self.screen_stack[-1]

    def move(self, directive):
        self.set_viewport(*self.move_translate.get(directive, (0,0)))

    def keep_viewport_inbounds(self):
        if self.vport_x < 0:
            self.vport_x = 0
        if self.vport_x >= self.active_screen.max_x:
            self.vport_x = self.active_screen.max_x
        if self.vport_y < 0:
            self.vport_y = 0
        if self.vport_y >= self.active_screen.max_y:
            self.vport_y = self.active_screen.max_y

    def increment_viewport(self, y, x):
        self.vport_x = self.vport_x+x
        self.vport_y = self.vport_y+y
        self.keep_viewport_inbounds()

    def set_viewport(self, y, x):
        self.vport_x = x
        self.vport_y = y
        self.keep_viewport_inbounds()

    def render(self):
        self.active_screen.screen.refresh(self.vport_y, self.vport_x,
                                          0, 0,
                                          self.window_max_y,
                                          self.window_max_x)
        curses.panel.update_panels()
        self.window.screen.refresh()

    def new_screen(self, screen_data, size_y=0, size_x=0):
        map_data = open(screen_data, 'r').readlines()
        max_y = size_y or len(map_data)
        max_x = size_x or max(map(len, map_data))
        screen = curses.newpad(max_y,max_x)
        screen.erase()
        screen.box()
        for y, line in enumerate(map_data):
            if y > max_y:
                break
            screen.addnstr(y, 0, line, max_x-1)
        return Screen(screen, max_y, max_x)

    def push_screen(self, screen):
        self.screen_stack.append(screen)

    def pop_screen(self, screen=None):
        if screen is not None:
            self.screen_stack.pop(self.screen_stack.index(screen))
        self.screen_stack.pop()

    def init_viewport(self):
        self.render()

    def run(self):
        background = self.new_screen("lines.txt")
        self.push_screen(background)
        self.init_viewport()

    def focus_active_screen(self):
        coords = self.focus_target.coords()
        screen_center = (ceil(float(self.window_max_y)/2),
                         ceil(float(self.window_max_x)/2))
        new_y = int(coords.get('y') - screen_center[0])
        new_x = int(coords.get('x') - screen_center[1])
        self.set_viewport(new_y, new_x)

    def update(self):
        self.focus_active_screen()
        self.render()


if __name__ == "__main__":
    d = ScreenManager()
    d.run()
    control_demo_keys = {"w": "up",
                         "a": "left",
                         "s": "down",
                         "d": "right"}
    while 1:
        cmd = d.active_screen.getkey()
        if cmd == "q":
            curses.endwin()
            sys.exit()
        d.move(control_demo_keys.get(cmd, ''))

