from threading import Thread
import logging
from sys import exit
from time import sleep
import pygame
from uuid import uuid4
try:
    from Queue import Queue
except ImportError:
    from queue import Queue

logger = logging.getLogger("soundManager")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("logs.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class soundManager(object):
    """ All sounds should be in ogg format at 44100 bitrate.
    sox inputfile.* -r 44100 output.ogg
    """
    manager = None

    def __init__(self):
        if soundManager.manager is not None:
            return soundManager.manager
        self.command_queue = Queue()
        self.is_running = True
        pygame.init()
        pygame.mixer.init(44100, 16, 2, 4096*4)
        self.sounds = {}
        self.commands = {"play": self.play}
        soundManager.manager = self

    def play(self, sound_hash, *args, **kwargs):
        print("playing sound: {}".format(self.sounds.get(sound_hash)))
        print(sound_hash)
        if isinstance(sound_hash, str):
            pygame.mixer.find_channel().queue(self.sounds.get(sound_hash))
        elif isinstance(sound_hash, list):
            for hash in sound_hash:
                pygame.mixer.find_channel().queue(self.sounds.get(hash))
        else:
            print(type(sound_hash))
        sleep(10)

    def register_sound(self, sound):
        hash = str(uuid4())
        self.sounds[hash] = pygame.mixer.Sound(sound)
        #pygame.mixer.music.play()
        return hash

    """
    def threaded_sound(self):
        try:
            while self.is_running:
                if not self.command_queue.empty():
                    command = self.command_queue.get_nowait()
                    self.commands[command["command"]](**command)
            sleep(0.001)
        except (KeyboardInterrupt, SystemExit):
            logger.info("killing thread")
            exit()

    def fire_sound_thread(self):
        self.T = Thread(target=self.threaded_sound)
        self.T.start()

    def stop_sound(self):
        logger.info("stopping sound thread")
        self.is_running = False
        while self.T.is_alive():
            logger.info("trying to join sound thread...")
            self.T.join(0.5)
    """
if __name__ == "__main__":
    sm = soundManager()
    #sm.fire_sound_thread()
    sounds = {"main": sm.register_sound("./yl.ogg")}
    sm.play(sounds["main"])
    print("done playing")
    sleep(10)
