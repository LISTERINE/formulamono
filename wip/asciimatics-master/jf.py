from asciimatics.effects import Sprite, Print
from asciimatics.particles import Rain
from asciimatics.event import KeyboardEvent, MouseEvent
from asciimatics.exceptions import ResizeScreenError
from asciimatics.renderers import StaticRenderer, SpeechBubble, FigletText
from asciimatics.screen import Screen
from asciimatics.paths import DynamicPath
from asciimatics.scene import Scene
import sys


class KeyboardController(DynamicPath):
    def process_event(self, event):
        if isinstance(event, KeyboardEvent):
            key = event.key_code
            if key == Screen.KEY_UP:
                self._y -= 1
                self._y = max(self._y, 2)
            elif key == Screen.KEY_DOWN:
                self._y += 1
                self._y = min(self._y, self._screen.height-2)
            elif key == Screen.KEY_LEFT:
                self._x -= 3
                self._x = max(self._x, 3)
            elif key == Screen.KEY_RIGHT:
                self._x += 3
                self._x = min(self._x, self._screen.width-3)
            else:
                return event
        else:
            return event


class TrackingPath(DynamicPath):
    def __init__(self, scene, path):
        super(TrackingPath, self).__init__(scene, 0, 0)
        self._path = path

    def process_event(self, event):
        return event

    def next_pos(self):
        x, y = self._path.next_pos()
        return x + 8, y - 2


class Map(Sprite):
    """
    Sample arrow sprite - points where it is going.
    """

    def __init__(self, screen, path, map_file, colour=Screen.COLOUR_WHITE, start_frame=0,
                 stop_frame=0):
        """
        See :py:obj:`.Sprite` for details.
        """
        super(Map, self).__init__(
            screen,
            renderer_dict={
                "default": StaticRenderer(images=[map_file]),
            },
            path=path,
            colour=colour,
            start_frame=start_frame,
            stop_frame=stop_frame)


def demo(screen):
    map_img = ""
    with open("lines.txt", "r") as map_data:
        map_img = "".join(map_data.readlines())
    map = Map(screen, KeyboardController(screen, 0, 0), map_img)
    scenes = []
    """
    effects = [
        Print(screen, FigletText("Hit the arrow with the mouse!", "digital"),
              y=screen.height//3-3),
        Print(screen, FigletText("Press space when you're ready.", "digital"),
              y=2 * screen.height//3-3),
    ]
    scenes.append(Scene(effects, -1))
    """
    effects = [
        map,
        Rain(screen, 10000)
    ]
    scenes.append(Scene(effects, -1))

    screen.play(scenes, stop_on_resize=True)


if __name__ == "__main__":
    while True:
        try:
            Screen.wrapper(demo)
            sys.exit(0)
        except ResizeScreenError:
            pass
