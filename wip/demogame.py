import curses
import sys

class Dungeon(object):
    def __init__(self):
        self.crs = curses.initscr()
        self.screen = curses.newpad(1000,1000)
        self.vport_x = 0
        self.vport_y = 0
        self.move_map = {"w": (-1,0),
                         "a": (0,-3),
                         "s": (1,0),
                         "d": (0,3)}

    def move(self, directive):
        self.set_viewport(*self.move_map.get(directive, (0,0)))

    def set_viewport(self, y, x):
        self.vport_x = self.vport_x+x
        self.vport_y = self.vport_y+y
        if self.vport_x < 0:
            self.vport_x = 0
        if self.vport_y < 0:
            self.vport_y = 0
        self.screen.refresh(self.vport_y, self.vport_x,
                            0, 0, self.max_y-2, self.max_x-2)

    def write_map(self):
        map_data = open("lines.txt", 'r').readlines()
        max_y, max_x = self.screen.getmaxyx()
        print max_y, max_x
        for y, line in enumerate(map_data):
            if y > max_y:
                break
            self.screen.addnstr(y, 0, line, max_x-1)

    def init_viewport(self):
        self.screen.refresh(0, 0,
                            0, 0, self.max_y, self.max_x)

    def init_cursor(self):
        self.screen.move(5,5)

    def init_dimensions(self):
        self.max_y, self.max_x = self.crs.getmaxyx()
        self.max_y = self.max_y-1
        self.max_x = self.max_x-1

    def run(self):
        self.init_dimensions()
        self.write_map()
        self.init_viewport()

if __name__ == "__main__":
    d = Dungeon()
    d.run()
    while 1:
        cmd = d.screen.getkey()
        if cmd == "q":
            curses.endwin()
            sys.exit()
        d.move(cmd)

