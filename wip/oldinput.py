import Queue
import threading
from readchar import readkey
from signal import signal, SIGINT
from sys import exit
from functools import partial


class KeyCapture(threading.Thread):
    def __init__(self, data_queue, control_queue, ctrlc=False):
            threading.Thread.__init__(self)
            self.data_queue = data_queue
            self.control_queue = control_queue

    @staticmethod
    def keyreader(data_queue):
        while 1:
            if not data_queue.empty():
                yield data_queue.get_nowait()

    def run(self):
        while True:
            if not self.control_queue.empty():
                command = self.control_queue.get_nowait()
                if command == "kill":
                    return
            print "waiting for key"
            plain_key = readkey()
            print "got plain key {}".format(plain_key)
            key = plain_key
            #key = self.key_lookup.get(plain_key, None)
            print "got key {}".format(key)
            if key is not None:
                self.data_queue.put(key)


if __name__ == "__main__":
    data_queue = Queue.Queue()
    control_queue = Queue.Queue()
    keycap = KeyCapture(data_queue, control_queue, ctrlc=True)
    keycap.start()
    keys = keycap.keyreader(data_queue)
    for key in keys:
        pass
    control_queue.put("kill")
