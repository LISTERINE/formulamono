from time import sleep
import curses, curses.panel

def make_panel(h,l, y,x, str):
 win = curses.newwin(h,l, y,x)
 win.erase()
 win.box()
 win.addstr(2, 2, str)

 panel = curses.panel.new_panel(win)
 return win, panel

def test(stdscr):
 try:
  curses.curs_set(0)
 except:
  pass
 pad = curses.newpad(1000,1000)
 #stdscr.box()
 #stdscr.addstr(2, 2, "panels everywhere")
 pad.addstr(2, 2, "panels everywhere")
 for i in range(38):
     pad.addstr(i, 0, "@"*60)
 pad.refresh(1, 1, 0, 0, 40, 60)
 curses.panel.update_panels(); stdscr.refresh()
 win1, panel1 = make_panel(10,12, 5,5, "Panel 1")
 win2, panel2 = make_panel(10,12, 8,8, "Panel 2")
 pad.refresh(1, 1, 0, 0, 40, 60)
 curses.panel.update_panels(); stdscr.refresh()
 sleep(1)

 panel1.top(); curses.panel.update_panels(); stdscr.refresh()
 sleep(1)

 for i in range(20):
  panel2.move(8, 8+i)
  pad.refresh(1, 1, 0, 0, 40, 60)
  curses.panel.update_panels(); stdscr.refresh()
  sleep(0.1)

 sleep(1)

if __name__ == '__main__':
 curses.wrapper(test)
